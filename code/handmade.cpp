#include "handmade.h"


internal void
GameOutputSound(game_sound_output_buffer *SoundBuffer, int ToneHz){
    local_persist real32 tSine;
    int16 ToneVolume = 3000;
    int WavePeriod = SoundBuffer->SamplesPerSecond/ToneHz;

    int16 *SampleOut = SoundBuffer->Samples;
    for(int SampleIndex=0; SampleIndex < SoundBuffer->SampleCount; ++SampleIndex){
        real32 SineValue = sinf(tSine);
        int16 SampleValue = (int16)(SineValue * ToneVolume);
        *SampleOut++ = SampleValue;
        *SampleOut++ = SampleValue;

        tSine += 2.0f * Pi32 * 1.0f / (real32)WavePeriod;
    }
}

internal void
RenderWeirdGradient(game_offscreen_buffer *Buffer, int BlueOffset, int GreenOffset){
    // NOTE(Rafik): Pixels are always 32-bits wide, Memory Order BB GG RR XX
    uint8 *Row = (uint8 *)Buffer->Memory;
    //uint8 i = 0;
    //int counter = 0;
    for(int Y = 0; Y < Buffer->Height; ++Y){
        uint32 *Pixel = (uint32 *)Row;
        for(int X = 0; X < Buffer->Width; ++X){
            uint8 Blue = (X + BlueOffset);
            uint8 Green = (Y + GreenOffset);
            uint8 Red = 255;

            //uint8 Blue = i;
            //uint8 Green = 0;
            //uint8 Red = 0;

            //counter++;
            //if(counter > 20){
            //    counter = 0;
            //    i++;
            //}
            *Pixel++ = (0 | (Red << 16) | (Green << 8) | Blue);
        }
        //i = 0;
			//counter = 0;
        Row += Buffer->Pitch;
    }
}

internal void
GameUpdateAndRender(game_memory *Memory, game_input *Input, game_offscreen_buffer *Buffer, game_sound_output_buffer *SoundBuffer){
    Assert(sizeof(game_state) <= Memory->PermanentStorageSize);

    game_state *GameState = (game_state *)Memory->PermanentStorage;

    if(!Memory->IsInitialized){
        char *Filename = __FILE__;
        debug_read_file_result File = DEBUGPlatformReadEntireFile(Filename);
        if(File.Contents){
            DEBUGPlatformWriteEntireFile("test2.out", File.ContentsSize, File.Contents);
            DEBUGPlatformFreeFileMemory(File.Contents);
        }
        GameState->ToneHz = 256;

        // TODO(Rafik): This may be more appropriate to do in the platform layer
        Memory->IsInitialized = true;
    }
    game_controller_input *Input0 = &Input->Controllers[0];
    if(Input0->IsAnalog){
        // NOTE(Rafik): Use analog movement tuning
        GameState->BlueOffset += (int)4.0f * (Input0->EndX);
        GameState->ToneHz = 256 + (int)(128.0f * (Input0->EndY));
    }
    else{
        // NOTE(Rafik): Use digital movement tuning
    }

    if(Input0->Down.EndedDown){
        GameState->GreenOffset += 1;
    }

    // TODO(Rafik): Allow sample offsets here for more robust platform options
    GameOutputSound(SoundBuffer, GameState->ToneHz);
	RenderWeirdGradient(Buffer, GameState->BlueOffset, GameState->GreenOffset);
}
